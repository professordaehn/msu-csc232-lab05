# Lab 5 - Extending the `ArrayBag`

## Getting Started

As usual:

* Fork this repo into your own, private, repo.
* Clone your private repo
* Checkout the develop branch before you begin your coding
* Commit, minimally, after implementing each of the three new operations
* After your final commit, create a pull request that seeks to merge _your_ develop branch into _your_ master branch
* Submit the URL of this pull request as a text submission for the Lab 5 assignment on Blackboard

## Goal

Your goal in this lab is simple: Create a class named `EnhancedArrayBag` that extends the class `ArrayBag` as shown in the following UML diagram, that passes all the unit tests.

![Bag class diagram](lab05-class-diagram.png "Bag class diagram.")

Notice that this new class 

* does not add any new attributes, but it does add three set operations, namely the ones fully described in HW01:
    * `unionWithBag(const EnhancedBag<ItemType>& other) const`
    * `intersectionWithBag(const EnhancedBag<ItemType>& other) const`
    * `differenceWithBag(const EnhancedBag<ItemType>& other) const`
* will not override any of the inherited methods
* must be implemented in separate header and implementation files whose names must be
    * `EnhancedArrayBag.h`
    * `EnhancedArrayBag.cpp`

## Generating Projects

As usual, this repo contains a number of `cmake` generator options. For example, navigate to the `build/unix` folder and run `cmake` to generate Unix make files that can be used to easily create executables from the source code.

```bash
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ make
... lots of output ...
$ ./Lab05
... lab executable output ...
$ ./Lab05_Test
... lots of output from the unit tests
$
```

NOTE: Upon initial checkout, running `make` will fail because the needed files/class declaration and defintion are missing. It's time to start building classes from scratch!
