/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    ArrayBag.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Header file for an array-based implementation of the ADT bag.
 *          Created by Frank M. Carrano and Timothy M. Henry.
 *
 * @copyright 2017 Pearson Education, Hoboken, New Jersey.
 */

#ifndef ARRAY_BAG_
#define ARRAY_BAG_

#include "BagInterface.h"

template<class ItemType>
class ArrayBag : public BagInterface<ItemType> {
private:
  static const int DEFAULT_CAPACITY = 10; // Small size to test for a full bag
  ItemType items[DEFAULT_CAPACITY];       // Array of bag items
  int itemCount;                          // Current count of bag items 
  int maxItems;                           // Max capacity of the bag
   
  // Returns either the index of the element in the array items that
  // contains the given target or -1, if the array does not contain 
  // the target.
  int getIndexOf(const ItemType& target) const;   

public:
  ArrayBag();
  virtual int getCurrentSize() const override;
  virtual bool isEmpty() const override;
  virtual bool add(const ItemType& newEntry) override;
  virtual bool remove(const ItemType& anEntry) override;
  virtual void clear() override;
  virtual bool contains(const ItemType& anEntry) const override;
  virtual int getFrequencyOf(const ItemType& anEntry) const override;
  virtual std::vector<ItemType> toVector() const override;
}; // end ArrayBag

#include "ArrayBag.cpp"

#endif
