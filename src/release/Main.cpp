/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include "ArrayBag.h"
#include "EnhancedArrayBag.h"

void displayBag(ArrayBag<std::string>& bag)
{
  std::cout << "The bag contains " << bag.getCurrentSize()
            << " items:" << std::endl;
  std::vector<std::string> bagItems = bag.toVector();
  int numberOfEntries = (int)bagItems.size();
  for (int i = 0; i < numberOfEntries; i++) {
    std::cout << bagItems[i] << " ";
  }  // end for
  std::cout << std::endl << std::endl;
} // end displayBag

void bagTester(ArrayBag<std::string>& bag) {
  std::cout << "isEmpty: returns " << bag.isEmpty()
            << "; should be 1 (true)" << std::endl;
  displayBag(bag);

  std::string items[] = {"one", "two", "three", "four", "five", "one"};
  std::cout << "Add 6 items to the bag: " << std::endl;
  for (int i = 0; i < 6; i++) {
    bag.add(items[i]);
  }  // end for
   
  displayBag(bag);

  std::cout << "isEmpty: returns " << bag.isEmpty()
            << "; should be 0 (false)" << std::endl;
   
  std::cout << "getCurrentSize: returns " << bag.getCurrentSize()
            << "; should be 6" << std::endl;

  std::cout << "Try to add another entry: add(\"extra\") returns "
            << bag.add("extra") << std::endl;
} // end bagTester

/**
 * @brief Entry point for the Lab 5 demo.
 * @param argc the number of command line arguments; unused by this demo.
 * @param argv an array of command line arguments; unused by this demo.
 * @return EXIT_SUCCESS is returned upon successful completion of this function.
 */
int main(int argc, char **argv) {
  ArrayBag<std::string> bag;
  std::cout << "Testing the Array-Based Bag:" << std::endl;
  std::cout << "The initial bag is empty." << std::endl;
  bagTester(bag);
  
  std::cout << std::endl << std::endl;
  std::cout << "Testing the Enhanced-Array-Based Bag1:" << std::endl;
  std::cout << "The initial bag is empty." << std::endl;
  EnhancedArrayBag<std::string> ebag1;
  bagTester(ebag1);

  std::cout << std::endl << std::endl;
  std::cout << "Testing the Enhanced-Array-Based Bag2:" << std::endl;
  std::cout << "The initial bag is empty." << std::endl;
  EnhancedArrayBag<std::string> ebag2;
  bagTester(ebag2);

  std::cout << std::endl << std::endl;
  std::cout << "Testing the Enhanced-Array-Based Bag3:" << std::endl;
  std::cout << "The initial bag is union of bag1 and bag2." << std::endl;
  EnhancedArrayBag<std::string> ebag3;
  ebag3 = ebag1.unionWithBag(ebag2);
  bagTester(ebag3);

  std::cout << std::endl << std::endl;
  EnhancedArrayBag<std::string> ebag4;
  ebag4.add("one");
  ebag4.add("two");
  ebag4.add("three");
  for (auto element : ebag4.toVector()) {
    std::cout << "element = " << element << std::endl;
  }

  std::cout << std::endl << std::endl;
  std::cout << "All done!" << std::endl;

  return EXIT_SUCCESS;
} // end main
